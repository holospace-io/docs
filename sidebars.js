module.exports = {
  someSidebar: {
    Basics: ["basics/intro", "basics/basics_camera"],
    Building: ["building/building_introduction", "building/building_audio"],
    Troubleshooting: ["troubleshooting/log_files"]
  },
};
