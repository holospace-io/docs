---
id: basics_camera
title: Controlling your camera
sidebar_label: Camera Controls
slug: /basics/camera
---

Holospace has two different camera modes that are useful depending on what you're doing in world.

#### Follow Mode

In follow mode, your camera automatically follows behind your avatar. It rotates when you turn your avatar, and automatically moves to avoid obstacles like walls or other players. This is the default camera mode, and you can return to this camera mode at any time by pressing `Escape`.

#### Fly Mode

In fly mode, your camera is decoupled from your avatar, and can focus on an object or other point in space. You can enter fly mode by holding `Alt` (or `Option` for macOS) and right clicking either an object, or somewhere on the ground.

---

## Manipulating the camera in fly mode

- To enter fly mode, hold `Alt` (or `Option` for macOS) and right click an object.
- To rotate your camera around your focal point, right click and drag your mouse around.
- To pan your camera in fly mode, hold `Shift`, right click, and drag your mouse around.
- To return to follow mode, press `Escape`.
