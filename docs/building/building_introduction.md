---
id: building_introduction
title: Building Introduction
sidebar_label: Introduction
slug: /building/introduction
---

Holospace allows you to create rich, immersive, and interactive creations that other users can enjoy. If you can dream it, you can almost certainly build it. Holospace makes it easy to import content from your device into the world, and the assemble it. If you have 3D models, images, or sounds, you can upload them and make them part of your creation.

---

## Creating your first object

Every creation in Holospace starts with a single cube, which you can then transform into whatever you want.

1. Start by clicking the **Build** button on the bottom toolbar.
2. Now, you'll see a toolbar at the top left of your screen. We'll explain the different tools shortly, but for now, click on the **Magic Wand**.
3. Click anywhere on the ground to place your cube.
4. Once placed, the inspector panel will automatically open on the right. You can use this panel to change all of the properties of your new object.

## Moving, rotating, and scaling an object

When in build mode, you'll see a toolbar at the top left of the screen that has five icons. They're described in detail below.

#### Create Tool (Magic Wand)

The create tool lets you place a new object wherever you click.

#### Transform Tool

The transform tool lets you move your object around on the X, Y, and Z axis. Use the red, green, and blue handles to move your object.

#### Rotate Tool

The rotate tool lets you rotate your object around the X, Y, and Z axis. Use the red, green, and blue rings to rotate your object.

#### Scale Tool

The scale tool lets you change the size of your object on the X, Y, and Z axis. Use the red, green, and blue handles to scale your object, or use the white handle at the middle to uniformly scale your object on all three axis.

#### Drag Tool

The drag tool lets you freely drag your object around the scene. This is the most intuitive way to position an object quickly. When the drag tool is selected, just left click and drag any object in the scene to move it. You can hold `Shift` to snap the object to its starting height if you don't want that to change.
