---
id: log_files
title: Viewing Log Files
sidebar_label: Viewing Log Files
slug: /troubleshooting/logs
---

If you encounter a bug in Holospace our support team may ask you to provide them with your log files. Your log 
files contain information useful to our developers such as more detailed information of why an error occurred, or where 
in our code it originated from. **Your logs do not contain any personal information,
such as your name, email, account login, etc.**

You can find your log file in the following locations depending on your operating system...

| OS | Log file location |
| ---------------- | ----------------- |
| **Windows**      | `C:\Users\username\AppData\LocalLow\Holospace\Holospace\Player.log` |
| **macOS**        | `~/Library/Logs/Holospace/Holospace/Player.log` |
| **Linux/Chrome OS** | `~/.config/unity3d/Holospace/Holospace/Player.log` |
